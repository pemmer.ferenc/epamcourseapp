import React from 'react';
import AuthorItem from '../components/AuthorItem/AuthorItem';

interface CourseAuthorsListProps {
  courseAuthors: any[];
  onDeleteAuthor: (author: any) => void;
}

const CourseAuthorsList: React.FC<CourseAuthorsListProps> = ({ courseAuthors, onDeleteAuthor }) => (
  <div>
    {courseAuthors.map(author => (
      <AuthorItem
        key={author.id}
        name={author.name}
        onButtonClick={() => onDeleteAuthor(author)}
        isCourseAuthor={true}
      />
    ))}
  </div>
);

export default CourseAuthorsList;
