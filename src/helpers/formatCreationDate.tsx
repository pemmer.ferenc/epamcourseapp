export const getFormattedCreationDate = (date: string): string => {
    const [day, month, year] = new Date(date).toLocaleDateString('en-GB').split('/');
    return `${day}.${month}.${year}`;
  };
  