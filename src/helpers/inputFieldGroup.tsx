import React from 'react';
import Input from '../common/Input/Input';

interface InputFieldGroupProps {
  title: string;
  description: string;
  onTitleChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  onDescriptionChange: (e: React.ChangeEvent<HTMLTextAreaElement>) => void;
  hasTitleError?: boolean;
  hasDescriptionError?: boolean;
}

const InputFieldGroup: React.FC<InputFieldGroupProps> = ({ title, description, onTitleChange, onDescriptionChange, hasTitleError, hasDescriptionError }) => (
  <>
    <Input
      labelText="Title"
      placeholderText="Enter course title"
      value={title}
      onChange={onTitleChange}
      hasError={hasTitleError}
    />
    {hasTitleError && <p className="error-message">Title is required.</p>}
    <textarea
      className={hasDescriptionError ? 'error' : ''}
      placeholder="Enter course description"
      value={description}
      onChange={onDescriptionChange}
    />
    {hasDescriptionError && <p className="error-message">Description is required.</p>}
  </>
);

export default InputFieldGroup;
