import React from 'react';

interface AuthorListProps {
  authors: any[];
  onAddAuthor: (author: any) => void;
}

const AuthorList: React.FC<AuthorListProps> = ({ authors, onAddAuthor }) => {
  return (
    <div>
      <h3>Authors</h3>
      {Array.isArray(authors) && authors.map(author => (
        <div key={author.id}>
          <span>{author.name}</span>
          <button onClick={() => onAddAuthor(author)}>Add</button>
        </div>
      ))}
    </div>
  );
};

export default AuthorList;
