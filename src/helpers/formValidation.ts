export const validateCourseForm = (title: string, description: string, duration: string, courseAuthors: any[]) => {
    const errors: { [key: string]: string } = {};
    if (!title) errors.title = 'Title is required';
    if (!description) errors.description = 'Description is required';
    if (!duration || parseInt(duration, 10) <= 0) errors.duration = 'Duration must be greater than 0';
    if (courseAuthors.length === 0) errors.courseAuthors = 'At least one author is required';
  
    return errors;
  };
  