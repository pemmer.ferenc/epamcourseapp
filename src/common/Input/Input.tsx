import React from 'react';
import './Input.css';

interface InputProps {
  labelText: string;
  placeholderText: string;
  value: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  type?: string;
  hasError?: boolean;
}

const Input: React.FC<InputProps> = ({ labelText, placeholderText, value, onChange, type = 'text', hasError = false }) => (
  <div className="input-container">
    <label>{labelText}</label>
    <input
      type={type}
      placeholder={placeholderText}
      value={value}
      onChange={onChange}
      className={hasError ? 'error' : ''}
    />
  </div>
);

export default Input;
