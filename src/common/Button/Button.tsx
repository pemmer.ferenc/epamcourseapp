import React from 'react';
import './Button.css';

interface ButtonProps {
  buttonText: string;
  onClick?: () => void;
  type?: 'button' | 'submit' | 'reset';
  className?: string;
  icon?: React.ReactNode; // Add icon prop
}

const Button: React.FC<ButtonProps> = ({ buttonText, onClick, type = 'button', className, icon }) => {
  return (
    <button type={type} onClick={onClick} className={className}>
      {icon && <span className="button-icon">{icon}</span>}
      {buttonText}
    </button>
  );
};

export default Button;
