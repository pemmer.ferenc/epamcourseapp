import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import { fetchCourses } from './store/courses/thunk';
import { fetchAuthors } from './store/authors/thunk';
import { fetchUserData } from './store/user/thunk';
import PrivateRoute from './components/PrivateRoute/PrivateRoute';
import Courses from './components/Courses/Courses';
import CourseForm from './components/CourseForm/CourseForm';
import Login from './components/UserAuthentication/Login/Login';
import Registration from './components/UserAuthentication/Registration/Registration';
import Header from './components/Header/Header';
import { RootState } from './store';

const App: React.FC = () => {
  const dispatch = useDispatch();
  const isLoggedIn = useSelector((state: RootState) => state.user.isAuth);

  useEffect(() => {
    dispatch(fetchCourses());
    dispatch(fetchAuthors());
    if (localStorage.getItem('token')) {
      dispatch(fetchUserData());
    }
  }, [dispatch]);

  return (
    <Router>
      <Header />
      <div>
        <Switch>
          <Route path="/login">
            {isLoggedIn ? <Redirect to="/courses" /> : <Login />}
          </Route>
          <Route path="/registration">
            {isLoggedIn ? <Redirect to="/courses" /> : <Registration />}
          </Route>
          <Route path="/courses" component={Courses} />
          <PrivateRoute path="/courses/add" component={CourseForm} />
          <PrivateRoute path="/courses/update/:courseId" component={CourseForm} />
          <Redirect from="/" to="/courses" />
        </Switch>
      </div>
    </Router>
  );
};

export default App;
