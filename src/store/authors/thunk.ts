import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import { RootState } from '..';
import { Author } from './types';

export const fetchAuthors = createAsyncThunk<Author[]>('authors/fetchAuthors', async () => {
  const response = await axios.get('/authors/all');
  return response.data;
});

export const addAuthor = createAsyncThunk<Author, { name: string }>(
  'authors/addAuthor',
  async (authorData, { getState }) => {
    const state = getState() as RootState;
    const response = await axios.post('/authors/add', authorData, {
      headers: {
        Authorization: `Bearer ${state.user.token}`,
      },
    });
    return response.data;
  }
);
