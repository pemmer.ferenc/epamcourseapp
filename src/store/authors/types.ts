export interface Author {
  id: string;
  name: string;
}

export interface AuthorsState {
  authors: Author[];
  loading: boolean;
  error: string | null;
}

export type AuthorState = Author[];
