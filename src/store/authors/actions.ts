import { createAsyncThunk } from '@reduxjs/toolkit';
import { getAuthors, addNewAuthor } from '../../services';
import { Author } from './types';

export const fetchAuthors = createAsyncThunk(
  'authors/fetchAuthors',
  async (_, thunkAPI) => {
    try {
      const response = await getAuthors();
      return response.result;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const createAuthor = createAsyncThunk(
  'authors/createAuthor',
  async (author: Author, thunkAPI) => {
    try {
      const response = await addNewAuthor(author);
      return response;
    } catch (error) {
      console.error('Error creating author:', error);
      return thunkAPI.rejectWithValue(error);
    }
  }
);
