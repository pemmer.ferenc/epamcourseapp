import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Author } from './types';
import { fetchAuthors, createAuthor } from './actions';

interface AuthorState {
  authors: Author[];
}

const initialState: AuthorState = {
  authors: []
};

const authorsSlice = createSlice({
  name: 'authors',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchAuthors.fulfilled, (state, action: PayloadAction<Author[]>) => {
      state.authors = action.payload;
    });
    builder.addCase(createAuthor.fulfilled, (state, action: PayloadAction<Author>) => {
      state.authors.push(action.payload);
    });
  }
});

export default authorsSlice.reducer;
