export interface Course {
  id: string;
  title: string;
  description: string;
  duration: number;
  creationDate: string;
  authors: string[];
}

export interface CoursesState {
  courses: Course[];
  loading: boolean;
  error: string | null;
}
