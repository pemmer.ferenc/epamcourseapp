import { createAsyncThunk } from '@reduxjs/toolkit';
import { getCourses, createCourseApi, deleteCourseApi } from '../../services';
import { Course } from './types';

export const fetchCourses = createAsyncThunk(
  'courses/fetchCourses',
  async (_, thunkAPI) => {
    try {
      const response = await getCourses();
      return response.result;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const createNewCourse = createAsyncThunk(
  'courses/createNewCourse',
  async (course: Course, thunkAPI) => {
    try {
      const response = await createCourseApi(course);
      return response;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const removeCourseById = createAsyncThunk(
  'courses/removeCourseById',
  async (id: string, thunkAPI) => {
    try {
      await deleteCourseApi(id);
      return id;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);
