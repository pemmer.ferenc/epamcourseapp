import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import { RootState } from '..';
import { Course } from './types';

export const fetchCourses = createAsyncThunk<Course[]>('courses/fetchCourses', async () => {
  const response = await axios.get('/courses/all');
  return response.data;
});

export const addCourse = createAsyncThunk<Course, Course>(
  'courses/addCourse',
  async (courseData, { getState }) => {
    const state = getState() as RootState;
    const response = await axios.post('/courses/add', courseData, {
      headers: {
        Authorization: `Bearer ${state.user.token}`,
      },
    });
    return response.data;
  }
);

export const updateCourse = createAsyncThunk<Course, { id: string; data: Course }>(
  'courses/updateCourse',
  async ({ id, data }, { getState }) => {
    const state = getState() as RootState;
    const response = await axios.put(`/courses/${id}`, data, {
      headers: {
        Authorization: `Bearer ${state.user.token}`,
      },
    });
    return response.data;
  }
);

export const deleteCourse = createAsyncThunk<string, string>(
  'courses/deleteCourse',
  async (id, { getState }) => {
    const state = getState() as RootState;
    await axios.delete(`/courses/${id}`, {
      headers: {
        Authorization: `Bearer ${state.user.token}`,
      },
    });
    return id;
  }
);
