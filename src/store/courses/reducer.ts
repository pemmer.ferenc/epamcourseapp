// store/courses/reducer.ts
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Course } from './types';
import { fetchCourses, createNewCourse, removeCourseById } from './actions';

interface CoursesState {
  items: any;
  courses: Course[];
}

const initialState: CoursesState = {
  courses: []
};

const coursesSlice = createSlice({
  name: 'courses',
  initialState,
  reducers: {
    setCourses(state, action: PayloadAction<Course[]>) {
      state.courses = action.payload;
    },
    addCourse(state, action: PayloadAction<Course>) {
      state.courses.push(action.payload);
    },
    deleteCourse(state, action: PayloadAction<string>) {
      state.courses = state.courses.filter(course => course.id !== action.payload);
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchCourses.fulfilled, (state, action: PayloadAction<Course[]>) => {
      state.courses = action.payload;
    });
    builder.addCase(createNewCourse.fulfilled, (state, action: PayloadAction<Course>) => {
      state.courses.push(action.payload);
    });
    builder.addCase(removeCourseById.fulfilled, (state, action: PayloadAction<string>) => {
      state.courses = state.courses.filter(course => course.id !== action.payload);
    });
  }
});

export const { setCourses, addCourse, deleteCourse } = coursesSlice.actions;

export default coursesSlice.reducer;
