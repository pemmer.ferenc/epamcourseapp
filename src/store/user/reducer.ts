import { createSlice } from '@reduxjs/toolkit';
import { UserState, userInitialState } from './types';
import { fetchUserData } from './thunk';

const userSlice = createSlice({
  name: 'user',
  initialState: userInitialState,
  reducers: {
    login(state, action) {
      state.isAuth = true;
      state.token = action.payload.token;
      state.name = action.payload.name;
      state.email = action.payload.email;
      state.role = action.payload.role;
    },
    logout(state) {
      state.isAuth = false;
      state.name = '';
      state.email = '';
      state.token = '';
      state.role = '';
      localStorage.removeItem('token');
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchUserData.fulfilled, (state, action) => {
      state.isAuth = true;
      state.name = action.payload.name;
      state.email = action.payload.email;
      state.role = action.payload.role;
    });
  },
});

export const { login, logout } = userSlice.actions;
export default userSlice.reducer;
