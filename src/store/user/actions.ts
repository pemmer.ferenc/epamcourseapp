// store/user/actions.ts
import { createAsyncThunk } from '@reduxjs/toolkit';
import { loginSuccess, logoutUser as logoutUserReducer } from './reducer';
import { fetchLogin } from '../../services';

export const loginUser = createAsyncThunk(
  'user/loginUser',
  async (credentials: { email: string; password: string }, thunkAPI) => {
    const response = await fetchLogin(credentials);
    if (response.status === 201) {
      const data = response.data;
      localStorage.setItem('token', `${data.result}`);
      thunkAPI.dispatch(loginSuccess(data.user));
      return data.user;
    } else {
      return thunkAPI.rejectWithValue(response.data);
    }
  }
);

export const logoutUser = createAsyncThunk('user/logoutUser', async (_, thunkAPI) => {
  thunkAPI.dispatch(logoutUserReducer());
});
