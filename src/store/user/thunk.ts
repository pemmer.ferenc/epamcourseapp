import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import { RootState } from '..';
import { UserState } from './types';

export const fetchUserData = createAsyncThunk<UserState, void, { state: RootState }>(
  'user/fetchUserData',
  async (_, { getState }) => {
    const state = getState();
    const response = await axios.get('/users/me', {
      headers: {
        Authorization: `Bearer ${state.user.token}`,
      },
    });
    return response.data;
  }
);

export const logoutUser = createAsyncThunk<void, void, { state: RootState }>(
  'user/logoutUser',
  async (_, { getState }) => {
    const state = getState();
    await axios.delete('/logout', {
      headers: {
        Authorization: `Bearer ${state.user.token}`,
      },
    });
  }
);
