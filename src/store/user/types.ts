export interface UserState {
  isAuth: boolean;
  name: string;
  email: string;
  token: string;
  role: string;
}

export const userInitialState: UserState = {
  isAuth: false,
  name: '',
  email: '',
  token: '',
  role: '',
};
