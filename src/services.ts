import axios from 'axios';
import { Author } from './store/authors/types';
import { Course } from './store/courses/types';

const API_URL = 'http://localhost:4000';

const getToken = () => {
  let token = localStorage.getItem('token');
  if (token && !token.startsWith('Bearer ')) {
    token = `Bearer ${token}`;
  }
  console.log('Fetched token:', token);
  return token || '';
};

export const fetchLogin = async (credentials: { email: string; password: string }) => {
  const response = await axios.post(`${API_URL}/login`, credentials, {
    headers: {
      'Content-Type': 'application/json',
    },
  });
  return response;
};

export const getCourses = async () => {
  const response = await axios.get(`${API_URL}/courses/all`, {
    headers: {
      Authorization: getToken(),
      'Content-Type': 'application/json',
    },
  });
  return response.data;
};

export const createCourseApi = async (course: Course) => {
  const response = await axios.post(`${API_URL}/courses/add`, course, {
    headers: {
      Authorization: getToken(),
      'Content-Type': 'application/json',
    },
  });
  return response.data;
};

export const deleteCourseApi = async (id: string) => {
  const response = await axios.delete(`${API_URL}/courses/${id}`, {
    headers: {
      Authorization: getToken(),
      'Content-Type': 'application/json',
    },
  });
  return response.data;
};

export const getAuthors = async () => {
  const response = await axios.get(`${API_URL}/authors/all`, {
    headers: {
      Authorization: getToken(),
      'Content-Type': 'application/json',
    },
  });
  return response.data;
};

export const addNewAuthor = async (author: Author) => {
  const token = getToken();
  console.log('Token:', token);
  console.log('Headers:', {
    Authorization: token,
    'Content-Type': 'application/json',
  });
  try {
    const response = await axios.post(`${API_URL}/authors/add`, author, {
      headers: {
        Authorization: token,
        'Content-Type': 'application/json',
      },
    });
    return response.data;
  } catch (error) {
    console.error('Error adding new author:', error);
    throw error; // Dobd el a hibát, hogy a hívó kód elkapja
  }
};
