// src/components/EmptyCourseList/EmptyCourseList.tsx
import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../store';
import { useNavigate } from 'react-router-dom';

const EmptyCourseList: React.FC = () => {
  const role = useSelector((state: RootState) => state.user.role);
  const navigate = useNavigate();

  const handleAddNewCourse = () => {
    if (role === 'ADMIN') {
      navigate('/courses/add');
    }
  };

  return (
    <div>
      {role === 'ADMIN' ? (
        <button onClick={handleAddNewCourse}>ADD NEW COURSE</button>
      ) : (
        <p>You don't have permissions to create a course. Please log in as ADMIN</p>
      )}
    </div>
  );
};

export default EmptyCourseList;
