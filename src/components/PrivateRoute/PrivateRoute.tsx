import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { RootState } from '../../store';

interface PrivateRouteProps {
  component: React.ComponentType<any>;
  path: string;
  exact?: boolean;
}

const PrivateRoute: React.FC<PrivateRouteProps> = ({ component: Component, ...rest }) => {
  const role = useSelector((state: RootState) => state.user.role);
  const isAuth = useSelector((state: RootState) => state.user.isAuth);

  return (
    <Route
      {...rest}
      render={(props) =>
        isAuth && role === 'ADMIN' ? <Component {...props} /> : <Redirect to="/courses" />
      }
    />
  );
};

export default PrivateRoute;
