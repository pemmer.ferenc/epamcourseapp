import React from 'react';
import { useParams, Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { RootState } from '../../store/rootReducer';
import Button from '../../common/Button/Button';
import { getFormattedCreationDate } from '../../helpers/formatCreationDate';
import { getCourseDuration } from '../../helpers/getCourseDuration';

interface CourseInfoProps {
  courses: any[];
}

const CourseInfo: React.FC<CourseInfoProps> = ({ courses }) => {
  const { courseId } = useParams<{ courseId: string }>();
  const course = courses.find(course => course.id === courseId);

  if (!course) {
    return <p>Course not found</p>;
  }

  const authors = useSelector((state: RootState) => state.authors.authors);

  const authorNames = course.authors.map((authorId: string) => {
    const author = authors.find((a: { id: string; }) => a.id === authorId);
    return author ? author.name : '';
  }).join(', ');

  return (
    <div>
      <h2>{course.title}</h2>
      <p><strong>ID:</strong> {course.id}</p>
      <p><strong>Description:</strong> {course.description}</p>
      <p><strong>Duration:</strong> {getCourseDuration(course.duration)}</p>
      <p><strong>Authors:</strong> {authorNames}</p>
      <p><strong>Creation Date:</strong> {getFormattedCreationDate(course.creationDate)}</p>
      <Link to="/courses">
        <Button buttonText="Back to Courses" />
      </Link>
    </div>
  );
};

export default CourseInfo;
