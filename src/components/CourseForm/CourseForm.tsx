// src/components/CourseForm/CourseForm.tsx
import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import InputFieldGroup from '../../helpers/inputFieldGroup';
import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import AuthorList from '../../helpers/AuthorList';
import CourseAuthorsList from '../../helpers/CourseAuthorsList';
import { v4 as uuidv4 } from 'uuid';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../store';
import { getFormattedCreationDate } from '../../helpers/formatCreationDate';
import { getCourseDuration } from '../../helpers/getCourseDuration';
import { validateCourseForm } from '../../helpers/formValidation';
import { fetchAuthors, addAuthor } from '../../store/authors/thunk';
import { addCourse, updateCourse } from '../../store/courses/thunk';
import './CourseForm.css';

interface Author {
  id: string;
  name: string;
}

const CourseForm: React.FC = () => {
  const dispatch = useDispatch<any>();
  const authors = useSelector((state: RootState) => state.authors.authors);
  const courses = useSelector((state: RootState) => state.courses.items);
  const { courseId } = useParams<{ courseId: string }>();
  const role = useSelector((state: RootState) => state.user.role);
  const navigate = useNavigate();

  const existingCourse = courses.find((course: { id: string | undefined; }) => course.id === courseId);

  const [title, setTitle] = useState(existingCourse ? existingCourse.title : '');
  const [description, setDescription] = useState(existingCourse ? existingCourse.description : '');
  const [duration, setDuration] = useState(existingCourse ? existingCourse.duration.toString() : '');
  const [courseAuthors, setCourseAuthors] = useState<Author[]>(existingCourse ? existingCourse.authors : []);
  const [newAuthor, setNewAuthor] = useState('');

  useEffect(() => {
    if (existingCourse) {
      setTitle(existingCourse.title);
      setDescription(existingCourse.description);
      setDuration(existingCourse.duration.toString());
      setCourseAuthors(existingCourse.authors);
    }
    dispatch(fetchAuthors());
  }, [existingCourse, dispatch]);

  if (role !== 'ADMIN') {
    return <p>Access denied. Only admins can create or update courses.</p>;
  }

  const handleAddAuthor = (author: Author) => {
    setCourseAuthors([...courseAuthors, author]);
  };

  const handleCreateAuthor = async () => {
    if (newAuthor.length < 2) return;
    const author: Author = { id: uuidv4(), name: newAuthor };
    try {
      await dispatch(addAuthor(author)).unwrap();
      setNewAuthor('');
    } catch (error: unknown) {
      console.error('Failed to create author:', error);
      alert('Failed to create author. Please try again.');
    }
  };

  const handleCreateOrUpdateCourse = async () => {
    const errors = validateCourseForm(title, description, duration, courseAuthors);
    if (Object.keys(errors).length > 0) {
      alert(Object.values(errors).join('\n'));
      return;
    }

    const newCourse = {
      id: existingCourse ? existingCourse.id : uuidv4(),
      title,
      description,
      creationDate: getFormattedCreationDate(new Date().toISOString()),
      duration: parseInt(duration, 10),
      authors: courseAuthors.map((author: Author) => author.id),
    };

    try {
      if (existingCourse) {
        await dispatch(updateCourse({ id: existingCourse.id, data: newCourse })).unwrap();
      } else {
        await dispatch(addCourse(newCourse)).unwrap();
      }
      navigate('/courses');
    } catch (error: unknown) {
      console.error('Failed to create or update course:', error);
      alert('Failed to create or update course. Please try again.');
    }
  };

  return (
    <div className='createCoursePage'>
      <h2>{existingCourse ? 'Edit Course' : 'Create Course'}</h2>
      <div className='createCourseForm'>
        <form>
          <h3>Main Info</h3>
          <InputFieldGroup
            title={title}
            description={description}
            onTitleChange={(e) => setTitle(e.target.value)}
            onDescriptionChange={(e) => setDescription(e.target.value)}
          />
          <div className='createCourseDuration'>
            <Input
              labelText="Duration (minutes)"
              placeholderText="Enter duration in minutes"
              value={duration}
              onChange={(e) => setDuration(e.target.value)}
              type="number"
            />
            <p>{duration ? getCourseDuration(parseInt(duration, 10)) : '00:00 hours'}</p>
          </div>
          <div className='createAuthorSection'>
            <div className='createAuthor'>
              <Input
                labelText="New Author"
                placeholderText="Enter author name"
                value={newAuthor}
                onChange={(e) => setNewAuthor(e.target.value)}
              />
              <Button buttonText="CREATE AUTHOR" onClick={handleCreateAuthor} />
            </div>
            <div className="createCourseAuthorsList">
              <h3>Course Authors</h3>
              {courseAuthors.length === 0 ? (
                <p>Author list is empty</p>
              ) : (
                <CourseAuthorsList courseAuthors={courseAuthors} onDeleteAuthor={handleAddAuthor} />
              )}
            </div>
          </div>
          <AuthorList authors={authors} onAddAuthor={handleAddAuthor} />
        </form>
      </div>
      <div className='createCourseActionsButtons'>
        <Button buttonText="CANCEL" onClick={() => navigate('/courses')} />
        <Button buttonText={existingCourse ? 'UPDATE COURSE' : 'CREATE COURSE'} onClick={handleCreateOrUpdateCourse} />
      </div>
    </div>
  );
};

export default CourseForm;
