// src/components/UserAuthentication/Registration/Registration.tsx
import React, { useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import Input from '../../../common/Input/Input';
import Button from '../../../common/Button/Button';
import './Registration.css';
import { useDispatch } from 'react-redux';
import { registerUser } from '../../../store/user/actions';

const Registration: React.FC = () => {
  const [name, setName] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [errors, setErrors] = useState<{ name?: string; email?: string; password?: string; submit?: string }>({});
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const validateForm = () => {
    const newErrors: { name?: string; email?: string; password?: string } = {};
    if (!name) {
      newErrors.name = 'Name is required';
    }
    if (!email) {
      newErrors.email = 'Email is required';
    } else if (!/\S+@\S+\.\S+/.test(email)) {
      newErrors.email = 'Email address is invalid';
    }
    if (!password) {
      newErrors.password = 'Password is required';
    } else if (password.length < 6) {
      newErrors.password = 'Password must be at least 6 characters';
    }
    return newErrors;
  };

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const newErrors = validateForm();
    if (Object.keys(newErrors).length > 0) {
      setErrors(newErrors);
      return;
    }

    const credentials = { name, email, password };

    try {
      const resultAction = await dispatch(registerUser(credentials) as any);
      console.log('Result action:', resultAction);
      if (registerUser.fulfilled.match(resultAction)) {
        navigate('/login');
      } else {
        console.error('Registration failed:', resultAction.payload);
        setErrors({ submit: 'Registration failed' });
      }
    } catch (error) {
      console.error('Error:', error);
      setErrors({ submit: 'Registration failed' });
    }
  };

  return (
    <div className="registration-container">
      <div className='title'>
        <h2>Register</h2>
      </div>
      <form onSubmit={handleSubmit}>
        <Input
          labelText="Name"
          placeholderText="Enter your name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        {errors.name && <p className="error-message">{errors.name}</p>}
        <Input
          labelText="Email"
          placeholderText="Enter your email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        {errors.email && <p className="error-message">{errors.email}</p>}
        <Input
          labelText="Password"
          placeholderText="Enter your password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          type="password"
        />
        {errors.password && <p className="error-message">{errors.password}</p>}
        <Button buttonText="Register" type="submit" />
        {errors.submit && <p className="error-message">{errors.submit}</p>}
        <p>
          Already have an account? <Link to="/login">Login</Link>
        </p>
      </form>
    </div>
  );
};

export default Registration;
