// src/components/UserAuthentication/Login/Login.tsx
import React, { useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import Input from '../../../common/Input/Input';
import Button from '../../../common/Button/Button';
import './Login.css';
import { useDispatch } from 'react-redux';
import { loginUser } from '../../../store/user/actions';

const Login: React.FC = () => {
  const [email, setEmail] = useState<string>('');
  const [password, setPassword] = useState<string>('');
  const [errors, setErrors] = useState<{ email?: string; password?: string; submit?: string }>({});
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const validateForm = () => {
    const newErrors: { email?: string; password?: string } = {};
    if (!email) {
      newErrors.email = 'Email is required';
    } else if (!/\S+@\S+\.\S+/.test(email)) {
      newErrors.email = 'Email address is invalid';
    }
    if (!password) {
      newErrors.password = 'Password is required';
    } else if (password.length < 6) {
      newErrors.password = 'Password must be at least 6 characters';
    }
    return newErrors;
  };

  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    const newErrors = validateForm();
    if (Object.keys(newErrors).length > 0) {
      setErrors(newErrors);
      return;
    }

    const credentials = { email, password };

    try {
      const resultAction = await dispatch(loginUser(credentials) as any);
      console.log('Result action:', resultAction);
      if (loginUser.fulfilled.match(resultAction)) {
        navigate('/courses');
      } else {
        console.error('Login failed:', resultAction.payload);
        setErrors({ submit: 'Login failed' });
      }
    } catch (error) {
      console.error('Error:', error);
      setErrors({ submit: 'Login failed' });
    }
  };

  return (
    <div className="login-container">
      <div className='title'>
        <h2>Login</h2>
      </div>
      <form onSubmit={handleSubmit}>
        <Input
          labelText="Email"
          placeholderText="Enter your email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        {errors.email && <p className="error-message">{errors.email}</p>}
        <Input
          labelText="Password"
          placeholderText="Enter your password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          type="password"
        />
        {errors.password && <p className="error-message">{errors.password}</p>}
        <Button buttonText="Login" type="submit" />
        {errors.submit && <p className="error-message">{errors.submit}</p>}
        <p>
          Don't have an account? <Link to="/register">Register</Link>
        </p>
      </form>
    </div>
  );
};

export default Login;
