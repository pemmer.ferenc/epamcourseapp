// src/components/Header/Header.tsx
import React from 'react';
import { useNavigate } from 'react-router-dom';
import Logo from './components/Logo/Logo';
import Button from '../../common/Button/Button';
import { useDispatch, useSelector } from 'react-redux';
import { logoutUser } from '../../store/user/thunk';
import './Header.css';
import { RootState } from '../../store';

const Header: React.FC = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { isAuth, name } = useSelector((state: RootState) => state.user);

  const handleLogout = async () => {
    try {
      await dispatch(logoutUser()).unwrap();
      navigate('/login');
    } catch (error) {
      console.error('Logout failed:', error);
    }
  };

  return (
    <header className="header">
      <Logo />
      <div className="header-controls">
        {isAuth ? (
          <>
            <span>{name}</span>
            <Button
              className="headerButton"
              buttonText="LOGOUT"
              onClick={handleLogout}
            />
          </>
        ) : (
          <Button
            className="headerButton"
              buttonText="LOGIN"
              onClick={() => navigate('/login')}
          />
        )}
      </div>
    </header>
  );
};

export default Header;
