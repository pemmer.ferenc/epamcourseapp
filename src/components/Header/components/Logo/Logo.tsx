import React from 'react';
import logo from "../../../../assets/logo.png"
import './Logo.css'

const Logo: React.FC = () => {
  return (
    <img className='logo' src={logo} alt="Logo" />
  );
};

export default Logo;
