import React from 'react';
import { FaPlus, FaTrashAlt } from 'react-icons/fa'; // Add the react-icons library

interface AuthorItemProps {
  name: string;
  onButtonClick: () => void;
  isCourseAuthor?: boolean;
}

const AuthorItem: React.FC<AuthorItemProps> = ({ name, onButtonClick, isCourseAuthor = false }) => {
  return (
    <div className="author-item">
      <span>{name}</span>
      {isCourseAuthor ? (
        <FaTrashAlt onClick={onButtonClick} className="icon" />
      ) : (
        <FaPlus onClick={onButtonClick} className="icon" />
      )}
    </div>
  );
};

export default AuthorItem;
