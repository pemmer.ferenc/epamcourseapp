// src/components/Courses/components/CourseCard/CourseCard.tsx
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../../../store';
import { deleteCourse } from '../../../../store/courses/thunk';
import { useNavigate } from 'react-router-dom';
import './courseCard.css';

interface CourseCardProps {
  id: string;
  title: string;
  description: string;
  duration: number;
  creationDate: string;
  authors: string[];
  onShowCourse: () => void;
  onDeleteCourse: () => void;
  onEditCourse: () => void;
}

const CourseCard: React.FC<CourseCardProps> = ({ id, title, description, duration, creationDate, authors, onShowCourse, onDeleteCourse, onEditCourse }) => {
  const role = useSelector((state: RootState) => state.user.role);

  return (
    <div className="course-card">
      <h3>{title}</h3>
      <p>{description}</p>
      <p>Duration: {duration} hours</p>
      <p>Created on: {creationDate}</p>
      <p>Authors: {authors.join(', ')}</p>
      <button onClick={onShowCourse}>Show Course</button>
      {role === 'ADMIN' && (
        <>
          <button onClick={onEditCourse}>Edit Course</button>
          <button onClick={onDeleteCourse}>Delete Course</button>
        </>
      )}
    </div>
  );
};

export default CourseCard;
