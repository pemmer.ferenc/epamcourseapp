import React from 'react';
import './Searchbar.css';
import Button from '../../../../common/Button/Button';

interface SearchBarProps {
  searchQuery: string;
  onSearchChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

const SearchBar: React.FC<SearchBarProps> = ({ searchQuery, onSearchChange }) => {
  return (
    <div className="searchBar">
      <input
        type="text"
        placeholder="Search..."
        value={searchQuery}
        onChange={onSearchChange}
      />
      <Button buttonText="SEARCH" onClick={() => {}} />
    </div>
  );
};

export default SearchBar;
