import React, { useEffect, useState } from 'react';
import CourseCard from './components/CourseCard/CourseCard';
import EmptyCourseList from '../EmptyCourseList/EmptyCourseList';
import SearchBar from './components/SearchBar/Searchbar';
import Button from '../../common/Button/Button';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { RootState } from '../../store';
import { fetchCourses } from '../../store/courses/thunk';
import { fetchUserData } from '../../store/user/thunk';
import './courses.css';

const Courses: React.FC = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const token = useSelector((state: RootState) => state.user.token);
  const user = useSelector((state: RootState) => state.user);
  const courses = useSelector((state: RootState) => state.courses.items);
  const [searchQuery, setSearchQuery] = useState<string>('');

  useEffect(() => {
    if (token) {
      dispatch(fetchUserData());
    }
  }, [dispatch, token]);

  useEffect(() => {
    dispatch(fetchCourses());
  }, [dispatch]);

  const filteredCourses = courses.filter(course =>
    course.title.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const handleAddNewCourse = () => {
    if (user.role === 'ADMIN') {
      navigate('/courses/add');
    } else {
      alert('You do not have permissions to create a course. Please log in as ADMIN.');
    }
  };

  return (
    <div className="courses">
      <div className="coursesHeader">
        <SearchBar searchQuery={searchQuery} onSearchChange={(e) => setSearchQuery(e.target.value)} />
        <Button buttonText="Add New Course" onClick={handleAddNewCourse} />
      </div>
      {filteredCourses.length === 0 ? (
        <EmptyCourseList />
      ) : (
        filteredCourses.map(course => (
          <CourseCard
            key={course.id}
            id={course.id}
            title={course.title}
            duration={course.duration}
            creationDate={course.creationDate}
            description={course.description}
            authors={course.authors}
            onShowCourse={() => navigate(`/courses/${course.id}`)}
            onDeleteCourse={() => dispatch(deleteCourse(course.id))}
            onEditCourse={() => navigate(`/courses/update/${course.id}`)}
          />
        ))
      )}
    </div>
  );
};

export default Courses;
